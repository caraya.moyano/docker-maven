FROM maven:3-eclipse-temurin-11
WORKDIR /app
CMD ["mvn", "clean", "install"]