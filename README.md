# Generar Contendor Maven


[![ Repositorio Git](https://gitlab.com/caraya.moyano/docker-maven.git)](https://gitlab.com/caraya.moyano/docker-maven.git)

## _Pre Requisitos_
```text
docker
```
## _Compilacion_

1- Clona el Proyecto.
```sh
git clone https://gitlab.com/caraya.moyano/docker-maven.git
```
2- compila la imagen:
```sh
docker build -t mi-imagen-maven:2.0 .
```
3- Obtener el path donde se ejecutara
```sh
pwd
```
4 - ejecuta Aplicacion (m2 en contenedor)
```sh
docker run -it -v ~RUTA_LOCAL(pwd)~:/app mi-imagen-maven:2.0 bash
```

4b - ejecuta Aplicacion (m2 en host)
```sh
docker run -it -v ~RUTA_LOCAL(pwd)~:/app -v ~RUTA_LOCAL_M2~:/root/.m2/ mi-imagen-maven:2.0 bash
```
